import java.awt.*;
import java.awt.Rectangle;

class Cell extends Rectangle{
    // fields
    int x;
    int y;
    static int size = 35;

    //constructors
    public Cell(int x, int y){
        super(x,y);
    }

    //methods
    void paint(Graphics g, Point mousePos){
        if(mousePos==null){
            return;
        }
        if(contains(mousePos)){
            g.setColor(Color.GRAY);
        } else {
            g.setColor(Color.WHITE);
        }
        g.fillRect(x,y,size,size);
        g.setColor(Color.BLACK);
        g.drawRect(x,y,size,size);
    }
}